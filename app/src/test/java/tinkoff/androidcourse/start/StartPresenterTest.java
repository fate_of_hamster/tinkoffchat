package tinkoff.androidcourse.start;

import android.content.Context;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import tinkoff.androidcourse.dagger.ComponentInjecter;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class StartPresenterTest {

    @Spy StartPresenter startPresenterSpy;
    @Mock StartView startViewMock;

    @Before
    public void setUp() throws Exception {
        ComponentInjecter.initComponent(mock(Context.class));
        MockitoAnnotations.initMocks(this);
        doReturn(startViewMock).when(startPresenterSpy).getView();
    }

    /* -- все эти тесты работали для проекта, оставшегося в ветке mvp; для текущей версии тесты пока написать не успед
    @Test
    public void when_attachView_and_result_null_do_nothing() throws Exception {
        startPresenterSpy.attachView(startViewMock);
        verify(startPresenterSpy, never()).
                setResult(any(StartTaskResult.class));
    }

    @Test
    public void when_attachView_and_result_not_null_call_onAuthorizationResult_and_clear_result() throws Exception {
        StartTaskResult result
                = startPresenterSpy.resultBuffer
                = new StartTaskResult(false, StartPresenter.MESSAGE_ERROR_START_TASK_EXECUTE);
        startPresenterSpy.attachView(startViewMock);
        verify(startPresenterSpy).
                setResult(result);
        assertNull(startPresenterSpy.resultBuffer);
    }

    @Test
    public void given_viewAttached_when_setAuthorizationResult_then_onAuthorizationResult_with_result() throws Exception {
        Mockito.doReturn(true).when(startPresenterSpy).isViewAttached();
        StartTaskResult result = new StartTaskResult(false, StartPresenter.MESSAGE_ERROR_START_TASK_EXECUTE);
        startPresenterSpy.onTaskResultReceived(result);
        verify(startPresenterSpy).setResult(result);
    }

    @Test
    public void given_not_viewAttached_when_setAuthorizationResult_then_set_authorizationResult() throws Exception {
        Mockito.doReturn(true).when(startPresenterSpy).isViewAttached();
        startPresenterSpy.onTaskResultReceived(new StartTaskResult(true));
        assertNull(startPresenterSpy.resultBuffer);
    }

    @Test
    public void given_result_true_from_start_task() throws Exception {
        Mockito.doReturn(true).when(startPresenterSpy).isViewAttached();
        startPresenterSpy.onTaskResultReceived(new StartTaskResult(true));

        verify(startViewMock).showLoadingStop();
        verify(startViewMock).onAuthorizationOk();
    }

    @Test
    public void given_result_false_from_start_task() throws Exception {
        Mockito.doReturn(true).when(startPresenterSpy).isViewAttached();
        StartTaskResult result = new StartTaskResult(false, StartPresenter.MESSAGE_ERROR_NEED_AUTHORIZATION);
        startPresenterSpy.onTaskResultReceived(result);

        verify(startViewMock).showLoadingStop();
        verify(startViewMock).showErrorMessage(result.getUserMessageID());
        verify(startViewMock).onAuthorizationError();
    }*/
}
