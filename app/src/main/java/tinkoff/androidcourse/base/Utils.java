package tinkoff.androidcourse.base;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class Utils {

    public static int DEBUG_DELAY_FOR_ASYNC_TASKS = 2000;
    
    public static void Log(String text) {
        Log.d("happy", text);
    }

    public static void showMessage(int resourcesID, Context context) {
        showMessage(context.getResources().getString(resourcesID), context);
    }

    public static void showMessage(String text, Context context) {
        Toast.makeText(
                context,
                text,
                (text.length() > 20 ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)
        ).show();
    }

    public static void showSnackbar(String messageText, View view) {
        Snackbar.make(view, messageText, Snackbar.LENGTH_SHORT).show();
    }

    public static void doDelay() {
        try {
            Thread.sleep(DEBUG_DELAY_FOR_ASYNC_TASKS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void hideKeyBoard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
}
