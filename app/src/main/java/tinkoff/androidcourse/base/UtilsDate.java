package tinkoff.androidcourse.base;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/*
перекинул один свой модуль, связанный с датами, из другого своего проекта
т.ч. тут есть методы, которые пока не задействованы в проекте
 */

public class UtilsDate {

    private static final String DATEFORMAT_DATE_AND_TIME = "dd MMMM HH:mm";    // 15 октября 12:05
    private static final String DATEFORMAT_ONLY_DATE = "dd.MM";                // 15.10
    private static final String DATEFORMAT_ONLY_DATE_WITH_YEAR = "dd.MM.yyyy"; // 15.10.2016
    private static final String DATEFORMAT_ONLY_TIME = "HH:mm";                // 12:05
    private static final String DATEFORMAT_DATE_BIRTH = "dd MMMM";             // 15 октября // формат для даты рождения


    public static Date getNowDate() {
        return Calendar.getInstance().getTime();
    }

    // разница между датами
    public static int getDiffDays(Date date1, Date date2) {
        return getDiffMinutes(getOnlyDate(date1), getOnlyDate(date2)) / (60 * 24);
    }
    public static int getDiffMinutes(Date date1, Date date2) {
        return (int) ( (date2.getTime() - date1.getTime()) / (1000 * 60));
    }

    // взять только дату (отрезает время)
    public static Date getOnlyDate(Date date) {
        return getDateAfterFormat(date, DATEFORMAT_ONLY_DATE_WITH_YEAR);
    }

    public static Date getOnlyTime(Date date) {
        return getDateAfterFormat(date, DATEFORMAT_ONLY_TIME);
    }

    // переконвертирование даты: дата в строку, и обратно из строки в дату (например, таким образом можно обрезать дату, или время)
    private static Date getDateAfterFormat(Date date, String formatName) {
        DateFormat format = new SimpleDateFormat(formatName);
        try {
            return format.parse(format.format(date)); // конвертируем в строку, и обратно в дату
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    // дата в строку красивую, для отображения пользователю
    public static String dateToStrForUser(Date date) {
        Date nowDate = Calendar.getInstance().getTime();
        int diffDay = getDiffDays(date, nowDate);
        int diffMinute = getDiffMinutes(date, nowDate);

        if ((diffMinute >= 0) && (diffMinute < 2)) {
            return "Только что";
        }
        else if ((diffMinute >= 2) && (diffMinute < 4)) {
            return "" + diffMinute + " минуты назад";
        }
        else if ((diffMinute >= 4) && (diffMinute < 10)) {
            return "" + diffMinute + " минут назад";
        }
        else if ((diffDay >= 0) && (diffDay < 1)) {
            return "Сегодня в " + dateToStrOnlyTime(date);
        }
        else if ((diffDay >= 1) && (diffDay < 2)) {
            return "Вчера в " + dateToStrOnlyTime(date);
        }
        else
            return dateToStr(date);
    }


    // дата в строку
    public static String dateToStr(Date date) {
        return dateToStr(date, DATEFORMAT_DATE_AND_TIME);
    }

    public static String dateToStr(Date date, String formatString) {
        if (date == null)
            return "";
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatString);
        return dateFormat.format(date);
    }

    public static String dateToStrOnlyTime(Date date) {
        return dateToStr(date, DATEFORMAT_ONLY_TIME);
    }

    public static String dateToStrOnlyDate(Date date) {
        return dateToStr(date, DATEFORMAT_ONLY_DATE);
    }

    public static String dateBirthToStr(Date date) {
        return dateToStr(date, DATEFORMAT_DATE_BIRTH);
    }

    // дата в строку, которую съест веб-сервис (дата тут в формате long)
    public static String dateLongToDateForWebService(long dateLong) {
        return "/Date(" + dateLong + "+0300)/";// todo: вот тут какая-то дребедень, я не пойму зачем мы +0300 добавляем, разве это не полетить по кочкам, если человек напишет сообщение, будучи в таиланде?
    }
    public static Date stringToDateForWebService(String dateString) {
        if (dateString != null)
            return new Date(Long.parseLong(dateString.replaceAll(".*?(\\d+).*", "$1")));
        return null;
    }

    // строка в дату
    public static Date strToDate(String dateStr) {
        if (dateStr == null)
            return null;
        long miliseconds = Long.parseLong(dateStr.replaceAll(".*?(\\d+).*", "$1"));
        return new Date(miliseconds);
    }


    // добавляет к дате указанное количество месяцев
    public static Date getDateAddMonth(int monthCount) {
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(System.currentTimeMillis());
        c.add(Calendar.MONTH, monthCount);
        return c.getTime();
    }


    //--------------------------------------------------------------------------------------- прочее
    // возвращает номер дня в неделе
    public static int getDayOfWeekNumber(Date date) {
        if (date != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            return c.get(Calendar.DAY_OF_WEEK);
        }
        return -1;
    }

    // возвращает названия дня недели
    public static String getDayOfWeekName(int dayOfWeekNumber) {
        String s = "";
        if (dayOfWeekNumber != -1)
            switch (dayOfWeekNumber) {
                case Calendar.MONDAY:
                    s = "пн";
                    break;
                case Calendar.TUESDAY:
                    s = "вт";
                    break;
                case Calendar.WEDNESDAY:
                    s = "ср";
                    break;
                case Calendar.THURSDAY:
                    s = "чт";
                    break;
                case Calendar.FRIDAY:
                    s = "пт";
                    break;
                case Calendar.SATURDAY:
                    s = "сб";
                    break;
                case Calendar.SUNDAY:
                    s = "вс";
                    break;
                default:
                    s = "";
                    break;
            }
        return s;
    }

    // определяет, является ли день концом недели
    public static Boolean getIsEndOfWeek(int dayOfWeekNumber) {
        if (dayOfWeekNumber != -1) {
            if ((dayOfWeekNumber == Calendar.SATURDAY)
                    || (dayOfWeekNumber == Calendar.SUNDAY))
                return true;
        }
        return false;
    }
}

