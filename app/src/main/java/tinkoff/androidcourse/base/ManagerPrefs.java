package tinkoff.androidcourse.base;

import android.content.Context;
import android.content.SharedPreferences;

public class ManagerPrefs {

    private static SharedPreferences sharedPreferences;

    public static final String PREFS_NAME = "MyPrefs";
    public static final String LAST_LOGIN = "LastLogin";
    public static final String IS_AUTH_OK = "IsAuthOk";
    public static final String CURRENT_USER_ID = "CurrentUserID";

    public ManagerPrefs(Context context) {
        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }



    public void saveLastLogin(String value){
        saveString(LAST_LOGIN, value);
    }
    public void clearLastLogin() {
        saveString(LAST_LOGIN, null);
    }
    public String getLastLogin(){
        return getString(LAST_LOGIN, null);
    }


    public boolean isAuthorizationOk(){
        return getBoolean(IS_AUTH_OK, false);
    }
    public void setAuthorizationOk(boolean isOK) {
        saveBoolean(IS_AUTH_OK, isOK);
    }

    public void saveCurrentUserID(int value){
        saveInt(CURRENT_USER_ID, value);
    }
    public void clearCurrentUserID() {
        remove(CURRENT_USER_ID);
    }
    public Integer getCurrentUserID(){
        return getInt(CURRENT_USER_ID, -1);
    }

    //----------------------------------------------------------------------------------------------
    private void saveString(String name, String value){
        this.sharedPreferences.edit().putString(name, value).commit();
    }
    private String getString(String name, String emptyValue){
        return this.sharedPreferences.getString(name, emptyValue);
    }

    private void saveBoolean(String name, boolean value){
        this.sharedPreferences.edit().putBoolean(name, value).commit();
    }
    private boolean getBoolean(String name, boolean emptyValue){
        return this.sharedPreferences.getBoolean(name, emptyValue);
    }

    private void saveInt(String name, int value){
        this.sharedPreferences.edit().putInt(name, value).commit();
    }
    private int getInt(String name, int emptyValue){
        return this.sharedPreferences.getInt(name, emptyValue);
    }

    private void remove(String name){
        this.sharedPreferences.edit().remove(name);
    }
}
