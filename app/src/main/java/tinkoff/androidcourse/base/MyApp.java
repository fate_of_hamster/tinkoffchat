package tinkoff.androidcourse.base;

import android.app.Application;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;

import tinkoff.androidcourse.dagger.ComponentInjecter;
import tinkoff.androidcourse.db_ormlite.HelperFactory;

public class MyApp  extends Application {

    @Inject
    public FirebaseDatabase firebaseDatabase;

    @Override
    public void onCreate(){
        super.onCreate();
        ComponentInjecter.initComponent(getApplicationContext()).inject(this);
        firebaseDatabase.setPersistenceEnabled(true);

        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
