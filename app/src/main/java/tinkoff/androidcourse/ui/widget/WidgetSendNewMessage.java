package tinkoff.androidcourse.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import tinkoff.androidcourse.R;

public class WidgetSendNewMessage extends RelativeLayout implements View.OnTouchListener {

    @BindView(R.id.edt_message) public EditText edtMessage;
    @BindView(R.id.btn_send) public ImageView btnSend;

    public WidgetSendNewMessage(Context context) {
        super(context);
    }

    public WidgetSendNewMessage(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public WidgetSendNewMessage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @Override
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouch(View v, MotionEvent event) {
        return true;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        edtMessage.setEnabled(enabled);
        btnSend.setEnabled(enabled);
    }

    //--------------------------------------------------------------------------------------- кнопка
    public void setOnButtonSendClickListener(@Nullable OnClickListener l) {
        btnSend.setOnClickListener(l);
    }

    //---------------------------------------------------------------------------------------- текст
    public Editable getText() {
        return edtMessage.getText();
    }
    public boolean isTextEmpty() {
        return edtMessage.getText().toString().isEmpty();
    }
    public void setText(String text) {
        edtMessage.setText(text);
    }
    public void clearText() {
        edtMessage.setText(null);
    }
    public void setHint(String hintText) {
        edtMessage.setHint(hintText);
    }

    //--------------------------------------------------------------------------------------- прочее
    private void init(AttributeSet attrs) {
        LayoutInflater.from(getContext()).inflate(R.layout.widget_send_message, this);
        ButterKnife.bind(this);
        initEdtMessage();
        initAttrs(attrs);
        setOnTouchListener(this);
    }

    private void initAttrs(AttributeSet attrs) {
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SendNewMessage);
        if (typedArray != null) {
            if (typedArray.getString(R.styleable.SendNewMessage_hint) != null)
                setHint(typedArray.getString(R.styleable.SendNewMessage_hint));
            setEnabled(typedArray.getBoolean(R.styleable.SendNewMessage_enabled, true));
            typedArray.recycle();
        }
    }

    private void initEdtMessage() {
        edtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setBtnSendEnabled(count > 0);
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void setBtnSendEnabled(boolean isEnabled) {
        btnSend.setEnabled(isEnabled);
        if (isEnabled)
            DrawableCompat.setTint(btnSend.getDrawable(), ContextCompat.getColor(getContext(), R.color.green));
        else
            DrawableCompat.setTint(btnSend.getDrawable(), ContextCompat.getColor(getContext(), R.color.disabled));

    }
}
