package tinkoff.androidcourse.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import tinkoff.androidcourse.R;
import tinkoff.androidcourse.db_ormlite.Message;

public class WidgetCloudMessage extends LinearLayout implements View.OnTouchListener {

    @BindView(R.id.cloud_message) public LinearLayout pnlCloudMessage;
    @BindView(R.id.img_user_photo) public ImageView imgUserPhoto;
    @BindView(R.id.lbl_user_name) public TextView lblUserName;
    @BindView(R.id.lbl_text) public TextView lblText;
    @BindView(R.id.lbl_create_date) public TextView lblCreateDate;
    private boolean isMyMessage = false;

    public WidgetCloudMessage(Context context) {
        super(context);
        init();
    }
    public WidgetCloudMessage(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(); // пока упрощаю, не обрабатываю attrs, т.к. это не надо сейчас
    }
    public WidgetCloudMessage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(); // пока упрощаю, не обрабатываю attrs, т.к. это не надо сейчас
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.widget_cloud_message, this);
        ButterKnife.bind(this);
        this.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        setOnTouchListener(this);
    }

    @Override
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouch(View v, MotionEvent event) {
        return true;
    }


    public void setUserPhoto(Bitmap value) {
        if (imgUserPhoto != null)
            imgUserPhoto.setImageBitmap(value);
    }

    public void setText(String value) {
        if (lblText != null)
            lblText.setText(value);
    }

    public void setUserName(String value) {
        if (lblUserName != null)
            lblUserName.setText(value);
    }

    public void setCreateDate(String value) {
        if (lblCreateDate != null)
            lblCreateDate.setText(value);
    }

    public void setIsMyMessage(boolean value) {
        isMyMessage = value;
        if (value) {
            this.setGravity(Gravity.RIGHT);
            pnlCloudMessage.setBackgroundResource(R.drawable.bgd_message_my);
        }
        else {
            this.setGravity(Gravity.LEFT);
            pnlCloudMessage.setBackgroundResource(R.drawable.bgd_message);
        }
    }

    public void setMessageStatus(int value) {
        int drawableID;
        switch (value) {
            case Message.STATUS_CREATE:
                drawableID = R.drawable.vector_ic_check;
                break;
            case Message.STATUS_SENDING:
                drawableID = R.drawable.vector_ic_spinner_of_dots; // не нравится мне эта иконка, но пока оставляю её
                break;
            case Message.STATUS_SENDED:
                drawableID = R.drawable.vector_ic_check_double;
                break;
            case Message.STATUS_NOT_SENDED:
                drawableID = R.drawable.vector_ic_cross;
                break;
            default:
                drawableID = R.drawable.vector_ic_check;
        }
        Drawable sendStatusDrawable = this.getResources().getDrawable(drawableID); // аналогично дагеру, не знаю, стоит ли в кастомной вьюхе лезть в ресурсы... хотя вроде ничего страшного
        if (lblCreateDate != null)
            lblCreateDate.setCompoundDrawablesWithIntrinsicBounds(null, null, sendStatusDrawable, null);
    }
}
