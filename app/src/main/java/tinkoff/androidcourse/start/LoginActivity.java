package tinkoff.androidcourse.start;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tinkoff.androidcourse.R;
import tinkoff.androidcourse.base.Utils;
import tinkoff.androidcourse.sign_up.SignUpActivity;
import tinkoff.androidcourse.ui.widget.WidgetProgressButton;


public class LoginActivity extends StartBaseActivity implements GoogleApiClient.OnConnectionFailedListener  {

    @BindView(R.id.edt_login) public EditText edtLogin;
    @BindView(R.id.edt_password) public EditText edtPassword;
    @BindView(R.id.btn_sign_in) public WidgetProgressButton btnSignIn;
    @BindView(R.id.btn_sign_in_with_google) public WidgetProgressButton btnSignInWithGoogle;
    @BindView(R.id.btn_sign_up_new_user) public WidgetProgressButton btnSignUpNewUser;

    private static final int RC_SIGN_IN = 1;
    private GoogleSignInOptions signInOptions;
    private GoogleApiClient client;

    private static final int RC_SIGN_UP = 2;


    public static void start(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(tinkoff.androidcourse.R.layout.login_activity);
        ButterKnife.bind(this);

        edtLogin.setText(presenter.getLastLogin());
        if (edtLogin.getText().toString().isEmpty())
            edtLogin.requestFocus();
        else
            edtPassword.requestFocus();

        presenter.setCurrentLoadingTypeToView();
    }


    // вход под существующим email'ом и паролем
    @OnClick(R.id.btn_sign_in)
    public void doSignIn() {
        if (edtLogin.getText().toString().isEmpty())
            Utils.showMessage(R.string.error_need_email, this);
        else if (edtPassword.getText().toString().isEmpty())
            Utils.showMessage(R.string.error_need_password, this);
        else {
            presenter.SignInExistingUser(
                    edtLogin.getText().toString(),
                    edtPassword.getText().toString());
            Utils.hideKeyBoard(this);
        }
    }


    // вход под гугл плюс аккаунтом
    @OnClick(R.id.btn_sign_in_with_google)
    public void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(getGoogleApiClient());
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private GoogleApiClient getGoogleApiClient() { // не понял пока что, надо ли это переносить в презентер, и возможно ли
        if (signInOptions == null) {
            signInOptions = new GoogleSignInOptions
                    .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(this.getResources().getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();
        }
        if (client == null) {
            client = new GoogleApiClient
                    .Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                    .build();
        }
        return client;
    }


    // регистрация нового пользователя
    @OnClick(R.id.btn_sign_up_new_user)
    public void SignUpNewUser() {
        SignUpActivity.start(this, RC_SIGN_UP);
    }
    private void SignUpNewUser(Intent data) {
        if (data != null) {
            String email = data.getStringExtra(SignUpActivity.PRMS_EMAIL);
            String password = data.getStringExtra(SignUpActivity.PRMS_PASSWORD);
            SignUpNewUser(email, password);
        }
    }
    private void SignUpNewUser(String email, String password) {
        if ((email != null) && (password != null) )
            presenter.SignUpNewUser(email, password);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN)
            presenter.SignInWithGooglePlus(data);
        else if ((requestCode == RC_SIGN_UP)
                && (resultCode == RESULT_OK))
            SignUpNewUser(data);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //empty
    }


    // операции с отображением индикаторов загрузки
    @Override
    public void showLoadingStartSignIn() {
        super.showLoadingStartSignIn();
        if (btnSignIn != null)
            btnSignIn.showProgress();
    }
    @Override
    public void showLoadingStartSignInWithGoogle() {
        super.showLoadingStartSignInWithGoogle();
        if (btnSignInWithGoogle != null)
            btnSignInWithGoogle.showProgress();
    }
    @Override
    public void showLoadingStartSignUpNewUser() {
        super.showLoadingStartSignUpNewUser();
        if (btnSignUpNewUser != null)
            btnSignUpNewUser.showProgress();
    }
    @Override
    public void showLoadingStop() {
        super.showLoadingStop();
        if (btnSignIn != null)
            btnSignIn.hideProgress();
        if (btnSignInWithGoogle != null)
            btnSignInWithGoogle.hideProgress();
        if (btnSignUpNewUser != null)
            btnSignUpNewUser.hideProgress();
    }
}
