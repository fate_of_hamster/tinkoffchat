package tinkoff.androidcourse.start;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import javax.inject.Inject;
import tinkoff.androidcourse.dagger.ComponentInjecter;
import tinkoff.androidcourse.base.ManagerPrefs;

public class StartPresenter extends MvpBasePresenter<StartView> {

    public static final int MESSAGE_ERROR_START_TASK_EXECUTE = 0;
    public static final int MESSAGE_ERROR_NEED_AUTHORIZATION = 1;
    public static final int MESSAGE_ERROR_LOGIN_EMPTY        = 2;
    public static final int MESSAGE_ERROR_PASSWORD_EMPTY     = 3;
    public static final int MESSAGE_ERROR_ADD_NEW_USER       = 4; // использую коды ошибок, что бы в данном презентере не было поползновений в R.string, т.к. посчитал это лишними связями

    @Inject public ManagerPrefs preferences;
    @Inject public FirebaseAuth firebaseAuth;
    @Inject public Context context;

    @VisibleForTesting StartTaskResult resultBuffer;

    public static final int LOADING_TYPE_STOP                = 0;
    public static final int LOADING_TYPE_SIGN_IN             = 1;
    public static final int LOADING_TYPE_SIGN_IN_WITH_GOOGLE = 2;
    public static final int LOADING_TYPE_SIGN_UP_NEW_USER    = 3;
    private int loadingType = LOADING_TYPE_STOP;


    public StartPresenter() {
        super();
        ComponentInjecter.getComponent().inject(this);
    }

    @Override
    public void attachView(StartView view) {
        super.attachView(view);
        if (resultBuffer != null) {
            setResult(resultBuffer);
            resultBuffer = null;
        }
    }

    public String getLastLogin() {
        return preferences.getLastLogin();
    }
    public boolean isAuthorizationOk() {
        return firebaseAuth.getCurrentUser() != null;
    }

    // процесс загрузки
    private boolean isLoadingTypeStop() {
        return this.loadingType == LOADING_TYPE_STOP;
    }
    private void setLoadingTypeStop() {
        setLoadingType(LOADING_TYPE_STOP);
    }
    private void setLoadingType(int loadingType) {
        this.loadingType = loadingType;
        setCurrentLoadingTypeToView();
    }
    public void setCurrentLoadingTypeToView() {
        if (isViewAttached()) {
            switch (this.loadingType) {
                case LOADING_TYPE_STOP:
                    getView().showLoadingStop();
                    break;
                case LOADING_TYPE_SIGN_IN:
                    getView().showLoadingStartSignIn();
                    break;
                case LOADING_TYPE_SIGN_IN_WITH_GOOGLE:
                    getView().showLoadingStartSignInWithGoogle();
                    break;
                case LOADING_TYPE_SIGN_UP_NEW_USER:
                    getView().showLoadingStartSignUpNewUser();
                    break;
            }
        }
    }

    // вход под существующим пользователем
    public void SignInExistingUser(String email, String password) {
        if ((isLoadingTypeStop())
                && (email != null)
                && (password != null)) {
            setLoadingType(LOADING_TYPE_SIGN_IN);
            firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(getOnCompleteListener());
        }
    }

    // вход под аккаунтом Google+
    public void SignInWithGooglePlus(Intent data) {
        SignInWithGooglePlus(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
    }
    private void SignInWithGooglePlus(GoogleSignInResult result) {
        if (result.isSuccess())
            SignInWithGooglePlus(result.getSignInAccount());
    }
    private void SignInWithGooglePlus(GoogleSignInAccount googleSignInAccount) {
        if (isLoadingTypeStop()) {
            setLoadingType(LOADING_TYPE_SIGN_IN_WITH_GOOGLE);
            AuthCredential credential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
            firebaseAuth.signInWithCredential(credential)
                    .addOnCompleteListener(getOnCompleteListener());
        }
    }

    // регистрация нового пользователя
    public void SignUpNewUser(String email, String password) {
        if ((isLoadingTypeStop())
                && (email != null)
                && (password != null))  {
            setLoadingType(LOADING_TYPE_SIGN_UP_NEW_USER);
            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(getOnCompleteListener());
        }
    }

    // слушатель окончания задачи
    private OnCompleteListener<AuthResult> getOnCompleteListener() {
        return new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                onAuthResultRecived(task);
            }
        };
    }

    // обработка полученного результата
    private void onAuthResultRecived(@NonNull Task<AuthResult> task) {
        setLoadingTypeStop();

        Integer messageID = null;
        String messageText = null;

        if (task.isSuccessful() == false) {
            if (task.getException() != null)
                messageText = task.getException().getMessage();
            else
                messageID = MESSAGE_ERROR_START_TASK_EXECUTE;
        }

        StartTaskResult result = new StartTaskResult(task.isSuccessful(), messageID, messageText);

        if (isViewAttached())
            setResult(result);
        else
            this.resultBuffer = result;
    }

    // установка результата
    public void setResult(StartTaskResult result) {
        setLoadingTypeStop();

        if (isViewAttached()) {
            if (result.getUserMessageID() != null)
                getView().showMessage(result.getUserMessageID());
            else if (result.getUserMessageString() != null)
                getView().showMessage(result.getUserMessageString());

            if (result.isAuthorizationOk())
                getView().onAuthorizationOk();
            else
                getView().onAuthorizationError();
        } else
            this.resultBuffer = result;
    }
}