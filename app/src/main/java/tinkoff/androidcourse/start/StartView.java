package tinkoff.androidcourse.start;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface StartView extends MvpView {

    void showLoadingStartSignIn();
    void showLoadingStartSignInWithGoogle();
    void showLoadingStartSignUpNewUser();
    void showLoadingStop();

    void onAuthorizationOk();
    void onAuthorizationError();

    void showMessage(Integer messageID);
    void showMessage(String message);
}
