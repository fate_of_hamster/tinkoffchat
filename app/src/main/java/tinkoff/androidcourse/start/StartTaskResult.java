package tinkoff.androidcourse.start;

public class StartTaskResult {

    private final boolean isAuthorizationOk;
    private final Integer userMessageID;
    private final String userMessageString;

    public StartTaskResult(boolean isAuthorizationOk, Integer userMessageID, String userMessageString) {
        this.isAuthorizationOk = isAuthorizationOk;
        this.userMessageID = userMessageID;
        this.userMessageString = userMessageString;
    }
    public StartTaskResult(boolean isAuthorizationOk) {
        this.isAuthorizationOk = isAuthorizationOk;
        this.userMessageID = null;
        this.userMessageString = null;
    }


    public boolean isAuthorizationOk() {
        return isAuthorizationOk;
    }

    public Integer getUserMessageID() {
        return userMessageID;
    }

    public String getUserMessageString() {
        return userMessageString;
    }
}
