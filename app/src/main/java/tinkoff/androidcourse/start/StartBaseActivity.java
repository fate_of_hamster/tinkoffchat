package tinkoff.androidcourse.start;

import android.os.Bundle;
import android.support.annotation.NonNull;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import tinkoff.androidcourse.main.MainActivity;
import tinkoff.androidcourse.R;
import tinkoff.androidcourse.base.Utils;

public class StartBaseActivity extends MvpActivity<StartView, StartPresenter> implements StartView  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public StartPresenter createPresenter() {
        return new StartPresenter();
    }

    @Override
    public void showLoadingStartSignIn() {
        // empty
    }
    @Override
    public void showLoadingStartSignInWithGoogle() {
        // empty
    }
    @Override
    public void showLoadingStartSignUpNewUser() {
        // empty
    }
    @Override
    public void showLoadingStop() {
        // empty
    }

    @Override
    public void onAuthorizationOk() {
        MainActivityStart();
    }
    @Override
    public void onAuthorizationError() {
        // empty
    }

    @Override
    public void showMessage(Integer messageID) {
        if (messageID != null) {
            Integer stringID = null;

            switch (messageID) {
                case StartPresenter.MESSAGE_ERROR_START_TASK_EXECUTE:
                    stringID = R.string.error_start_task_execute;
                    break;
                case StartPresenter.MESSAGE_ERROR_NEED_AUTHORIZATION:
                    stringID = R.string.error_need_authorization;
                    break;
                case StartPresenter.MESSAGE_ERROR_LOGIN_EMPTY:
                    stringID = R.string.error_email_empty;
                    break;
                case StartPresenter.MESSAGE_ERROR_PASSWORD_EMPTY:
                    stringID = R.string.error_password_empty;
                    break;
                case StartPresenter.MESSAGE_ERROR_ADD_NEW_USER:
                    stringID = R.string.error_add_new_user;
                    break;
            }

            if (stringID != null)
                Utils.showMessage(stringID, this);
        }
    }

    @Override
    public void showMessage(String message) {
        Utils.showMessage(message, this);
    }

    public void LoginActivityStart() {
        LoginActivity.start(this);
        finish();
    }
    public void MainActivityStart() {
        MainActivity.start(this);
        finish();
    }
}