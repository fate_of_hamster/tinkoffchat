package tinkoff.androidcourse.start;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import tinkoff.androidcourse.R;

public class SplashActivity extends StartBaseActivity {

    @BindView(R.id.main_logo) public ImageView main_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (presenter.isAuthorizationOk()) {
            setContentView(R.layout.splash_activity);
            ButterKnife.bind(this);
            // здесь можно попробовать диалоги подгрузить, что бы зря стартовый экран не показывать; но это позже
            onAuthorizationOk();
        }
        else
            onAuthorizationError();
    }

    @Override
    public void onAuthorizationError() {
        super.onAuthorizationError();
        LoginActivityStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startAnimation();
    }

    private void startAnimation() {
        if ((main_logo != null)
                && (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)) {
            Drawable drawable = main_logo.getDrawable();
            if (drawable instanceof Animatable)
                ((Animatable) drawable).start();
        }
    }
}