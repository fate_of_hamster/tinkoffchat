package tinkoff.androidcourse.main;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import javax.inject.Inject;
import tinkoff.androidcourse.dagger.ComponentInjecter;

public class MainPresenter extends MvpBasePresenter<MainView> {

    @Inject public FirebaseAuth firebaseAuth;

    public MainPresenter() {
        super();
        ComponentInjecter.getComponent().inject(this);
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        refillCurrentUserName();
    }

    public void refillCurrentUserName() {
        if (isViewAttached())
            getView().setCurrentUserName(getCurrentUserName());
    }

    private String getCurrentUserName() {
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            String userName = currentUser.getDisplayName();
            if (userName == null)
                userName = currentUser.getEmail(); // будем считать, что пользователь согласен, что бы другие видели его email
            return userName;
        } else
            return null;
    }

    public void signOut() {
        firebaseAuth.signOut();
        if (isViewAttached())
            getView().goToLoginActivity();
    }
}