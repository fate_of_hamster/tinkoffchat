package tinkoff.androidcourse.main;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface MainView extends MvpView {

    void setCurrentUserName(String value);
    void goToLoginActivity();

}
