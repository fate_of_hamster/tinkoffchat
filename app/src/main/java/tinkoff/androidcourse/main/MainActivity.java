package tinkoff.androidcourse.main;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import tinkoff.androidcourse.R;
import tinkoff.androidcourse.about_app.AboutAppFragment;
import tinkoff.androidcourse.base.Utils;
import tinkoff.androidcourse.room.room_dict.RoomDictFragment;
import tinkoff.androidcourse.settings.SettingsFragment;
import tinkoff.androidcourse.start.LoginActivity;

public class MainActivity  extends MvpActivity<MainView, MainPresenter> implements NavigationView.OnNavigationItemSelectedListener, MainView {

    @BindView(R.id.toolbar) public Toolbar toolbar;
    @BindView(R.id.drawer_layout) public DrawerLayout drawer;
    @BindView(R.id.nav_view) public NavigationView navigationView;

    private static final String STATE_TITLE = "title";

    private ActionBarDrawerToggle toggle;
    private final static int MENU_DIALOGS = 0;
    private String nowShowFragmentTag;
    private long lastTimeBackPresed;
    private static final long TIME_INTERVAL_TO_EXIT = 2000;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, 0, 0);
        drawer.addDrawerListener(toggle);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            navigationView.getMenu().getItem(MENU_DIALOGS).setChecked(true);
            onNavigationItemSelected(navigationView.getMenu().getItem(MENU_DIALOGS));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.refillCurrentUserName();
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_TITLE, getSupportActionBar().getTitle().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        getSupportActionBar().setTitle(savedInstanceState.getString(STATE_TITLE));
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else if ((getSupportFragmentManager().getBackStackEntryCount() == 0)
                && (System.currentTimeMillis() - lastTimeBackPresed >= TIME_INTERVAL_TO_EXIT)) {
            lastTimeBackPresed = System.currentTimeMillis();
            Utils.showSnackbar(this.getString(R.string.again_press_back_for_exit), drawer);
        }
        else
            super.onBackPressed();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_dialogs:
                showFragment(RoomDictFragment.TAG);
                break;
            case R.id.nav_settings:
                showFragment(SettingsFragment.TAG);
                break;
            case R.id.nav_about:
                showFragment(AboutAppFragment.TAG);
                break;
            case R.id.nav_exit:
                presenter.signOut();
                break;
        }
        item.setChecked(true);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }


    private void showFragment(String fragmentTag) {
        if ((nowShowFragmentTag == null)
                || (!nowShowFragmentTag.equals(fragmentTag))) {
            int titleStringID = -1;
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);

            if (fragment == null) {
                switch (fragmentTag) {
                    case RoomDictFragment.TAG:
                        fragment = RoomDictFragment.newInstance();
                        titleStringID = RoomDictFragment.NAME_STRING_ID;
                        break;
                    case SettingsFragment.TAG:
                        fragment = SettingsFragment.newInstance();
                        titleStringID = SettingsFragment.NAME_STRING_ID;
                        break;
                    case AboutAppFragment.TAG:
                        fragment = AboutAppFragment.newInstance();
                        titleStringID = AboutAppFragment.NAME_STRING_ID;
                        break;
                }
            }

            if (fragment != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.content_navigation, fragment, fragmentTag)
                        .commit();
                nowShowFragmentTag = fragmentTag;
                if ((getSupportActionBar() != null)
                        && (titleStringID > -1))
                    getSupportActionBar().setTitle(this.getString(titleStringID));
            }
        }
    }

    @Override
    public void goToLoginActivity() {
        LoginActivity.start(this);
        this.finish();
    }

    @Override
    public void setCurrentUserName(String value) {
        if ((navigationView != null) && (value != null)) {
            TextView userName = ButterKnife.findById(navigationView.getHeaderView(0), R.id.user_name);
            if (userName != null)
                userName.setText(value);
        }
    }
}
