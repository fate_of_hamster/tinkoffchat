package tinkoff.androidcourse.sign_up;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tinkoff.androidcourse.R;
import tinkoff.androidcourse.base.Utils;

/*
Пока делаю без презентера, и просто возвращаю что ввели в запустившее нас активити
Просто потому что не успеваю всё сделать и со всем разобраться за несколько вечеров
 */

public class SignUpActivity extends AppCompatActivity {

    public static final String PRMS_EMAIL = "PRMS_EMAIL";
    public static final String PRMS_PASSWORD = "PRMS_PASSWORD";

    @BindView(R.id.edt_email) public EditText edtEmail;
    @BindView(R.id.edt_password) public EditText edtPassword;


    public static void start(Activity activityWaitResult, int requestCode) {
        Intent intent = new Intent(activityWaitResult, SignUpActivity.class);
        activityWaitResult.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_activity);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle(getString(R.string.sign_up_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_sign_up)
    public void onSignUpClick() {
        if (edtEmail.getText().toString().isEmpty())
            Utils.showMessage(R.string.error_need_email, this);
        else if (edtPassword.getText().toString().isEmpty())
            Utils.showMessage(R.string.error_need_password, this);
        else {
            // я пока не буду заморачиваться, просто верну что ввели в активити родителя; просто потому что мало времени до финальной сдачи курсового
            // да, это неудобно, но у меня сейчас есть более приоритеные проблемы; так что пока оставляю так
            Intent intent = new Intent();
            intent.putExtra(PRMS_EMAIL, edtEmail.getText().toString());
            intent.putExtra(PRMS_PASSWORD, edtPassword.getText().toString());
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
