package tinkoff.androidcourse.about_app;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tinkoff.androidcourse.R;

public class AboutAppFragment extends Fragment {

    public static final String TAG = "AboutAppFragment";
    public static final int NAME_STRING_ID = R.string.fragment_title_about_app;

    @BindView(R.id.about_app_text) public TextView aboutAppText;
    @BindView(R.id.main_logo_panel) public RelativeLayout mainLogoPanel;
    @BindView(R.id.main_logo) public ImageView imgMainLogo;
    @BindView(R.id.lbl_app_name) public TextView lblAppName;
    private String aboutAppInfo;

    public static AboutAppFragment newInstance() {
        AboutAppFragment fragment = new AboutAppFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_app_fragment, container, false);
        ButterKnife.bind(this, view);
        showAboutAppInfoOnDisplay();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        imgMainLogo.setVisibility(View.INVISIBLE);
        lblAppName.setVisibility(View.INVISIBLE);
        startLogoAnimationWithDellay();
    }

    private void showAboutAppInfoOnDisplay() {
        if (aboutAppInfo == null)
            aboutAppInfo = getAboutAppInfo();

        aboutAppText.setText(aboutAppInfo);
    }


    private String getAboutAppInfo() {
        String result;
        PackageInfo packageInfo;
        try {
            packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            result = packageInfo.versionName +"." + String.valueOf(packageInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            result = this.getString(tinkoff.androidcourse.R.string.error_get_version_app);
        }
        result = String.format(this.getString(tinkoff.androidcourse.R.string.app_about_description), result);
        return result;
    }


    // аналогично, анимации уделил не так много времени, как хотелось бы
    // так что, пока всё в духе минимализма, на майских попробую допилить красоту
    // пока надо другие технические нюансы допилить
    // + не нашёл пока что как при использовании TransitionManager делать задержку, сделал грубо через AsyncTask
    public void startLogoAnimationWithDellay() {
        new animationAsyncTask().execute();
    }
    public void startLogoAnimation() {
        if ((mainLogoPanel != null)
                && (imgMainLogo != null)
                && (lblAppName != null)) {
            TransitionManager.beginDelayedTransition(mainLogoPanel);
            imgMainLogo.setVisibility(View.VISIBLE);
            lblAppName.setVisibility(View.VISIBLE);
        }
    }
    private class animationAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                Thread.sleep(500); // подождём 0,5 секунды
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            startLogoAnimation();
        }
    }
}
