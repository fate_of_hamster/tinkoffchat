package tinkoff.androidcourse.dagger;

import javax.inject.Singleton;
import dagger.Component;
import tinkoff.androidcourse.base.MyApp;
import tinkoff.androidcourse.storage.StorageManager;
import tinkoff.androidcourse.main.MainPresenter;
import tinkoff.androidcourse.room.room_card.RoomPresenter;
import tinkoff.androidcourse.room.room_create.RoomCreatePresenter;
import tinkoff.androidcourse.room.room_dict.RoomDictPresenter;
import tinkoff.androidcourse.start.StartPresenter;
import tinkoff.androidcourse.room.room_card.MessagesAdapter;

@Singleton
@Component(modules = {
        ModuleStorage.class,
        ModuleFirebase.class,
        ModuleAndroid.class})
public interface ComponentApp {
    void inject(MyApp myApp);

    void inject(StartPresenter startPresenter);
    void inject(MainPresenter mainPresenter);
    void inject(RoomCreatePresenter roomCreatePresenter);
    void inject(RoomDictPresenter roomDictPresenter);
    void inject(RoomPresenter roomPresenter);

    void inject(StorageManager storageManager);

    void inject(MessagesAdapter adapter);
}
