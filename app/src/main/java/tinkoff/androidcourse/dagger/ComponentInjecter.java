package tinkoff.androidcourse.dagger;

import android.content.Context;

public class ComponentInjecter {

    private static ComponentApp componentApp;

    public static ComponentApp initComponent(Context context) {
        ModuleStorage moduleStorage = new ModuleStorage();
        ModuleFirebase moduleFirebase = new ModuleFirebase();
        ModuleAndroid moduleAndroid = new ModuleAndroid(context);
        componentApp = DaggerComponentApp
                .builder()
                .moduleStorage(moduleStorage)
                .moduleAndroid(moduleAndroid)
                .moduleFirebase(moduleFirebase)
                .build();
        return componentApp;
    }

    public static ComponentApp getComponent() {
       if (componentApp == null)
            throw new NullPointerException("ComponentInjecter: ComponentApp не инициализирован"); // хм, не знаю, нужно ли это тут
        return componentApp;
    }
}
