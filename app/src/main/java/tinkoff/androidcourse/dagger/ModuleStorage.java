package tinkoff.androidcourse.dagger;

import android.content.Context;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import tinkoff.androidcourse.base.ManagerPrefs;

@Module
public class ModuleStorage {

    @Provides
    @Singleton
    ManagerPrefs providesManagerPrefs(Context context) {
        return new ManagerPrefs(context);
    }
}