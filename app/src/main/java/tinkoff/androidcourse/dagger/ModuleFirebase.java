package tinkoff.androidcourse.dagger;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import tinkoff.androidcourse.storage.StorageManager;

@Module
public class ModuleFirebase {

    @Provides
    @Singleton
    FirebaseAuth providesFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }

    @Provides
    @Singleton
    FirebaseDatabase providesFirebaseDatabase() {
        return FirebaseDatabase.getInstance();
    }

    @Provides
    @Singleton
    StorageManager providesFirebaseManager() {
        return new StorageManager();
    }
}