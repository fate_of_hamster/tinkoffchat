package tinkoff.androidcourse.storage;

import java.util.List;

public interface MessageItemValueListener {
    void onValue(List<MessageItem> items);
    void onAdd(MessageItem item);
    void showMessage(String text);
}
