package tinkoff.androidcourse.storage;

public interface OnTransactionComplete<T> {
    void onCommit(T result);
    void onAbort(Exception e);
}
