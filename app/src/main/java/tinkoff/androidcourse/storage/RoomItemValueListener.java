package tinkoff.androidcourse.storage;

import java.util.List;

public interface RoomItemValueListener {
    void onValue(List<RoomItem> items);
    void showMessage(String text);
}
