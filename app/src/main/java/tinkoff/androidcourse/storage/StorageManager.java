package tinkoff.androidcourse.storage;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import javax.inject.Inject;
import tinkoff.androidcourse.base.Utils;
import tinkoff.androidcourse.base.UtilsDate;
import tinkoff.androidcourse.dagger.ComponentInjecter;


public class StorageManager {

    public static final String NODE_ROOMS = "rooms";
    public static final String NODE_MESSAGES = "messages";

    @Inject public FirebaseDatabase firebaseDatabase;
    @Inject public FirebaseAuth firebaseAuth;
    private final DatabaseReference dbRooms;
    private final DatabaseReference dbMessages;
    private final FirebaseUser currentUser;
    private LiteChildEventListener messageListener;


    public StorageManager() {
        ComponentInjecter.getComponent().inject(this);
        this.currentUser = firebaseAuth.getCurrentUser();
        this.dbRooms = firebaseDatabase.getReference(NODE_ROOMS);
        this.dbMessages = firebaseDatabase.getReference(NODE_MESSAGES);
    }

    public static Transaction.Handler getAddItemTransactionHandler(final Object item, final OnTransactionComplete<Void> onTransactionComplete) {
        return new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                mutableData.setValue(item);
                return Transaction.success(mutableData);
            }
            @Override
            public void onComplete(DatabaseError databaseError, boolean commited, DataSnapshot dataSnapshot) {
                if (commited)
                    onTransactionComplete.onCommit(null);
                else
                    onTransactionComplete.onAbort(databaseError.toException());
            }
        };
    }

    public String getCurrentUserUID() {
        return currentUser.getUid();
    }

    //-------------------------------------------------------------------------------------- комнаты
    // добавление
    public void addRoom(String name, final OnTransactionComplete<Void> onTransactionComplete) {
        RoomItem item = new RoomItem(name, currentUser.getUid(), UtilsDate.getNowDate(), null);
        dbRooms.push().runTransaction(
                        getAddItemTransactionHandler(item, onTransactionComplete));
    }

    // слушатели
    public void setRoomListener(final RoomItemValueListener eventListener) {
        dbRooms.addChildEventListener(
                new LiteChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        RoomItem value = dataSnapshot.getValue(RoomItem.class);
                        value.setUID(dataSnapshot.getKey());
                        Utils.Log("on room add: " + value.toString());
                    }
                });
    }

    public void getRooms(final RoomItemValueListener eventListener) {
        dbRooms.addListenerForSingleValueEvent(
                new LiteValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ArrayList<RoomItem> items = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            RoomItem item = snapshot.getValue(RoomItem.class);
                            String uid = snapshot.getKey();
                            item.setUID(uid);
                            items.add(item);
                        }
                        eventListener.onValue(items);
                    }
                }
        );
    }


    //------------------------------------------------------------------------------------ сообщения
    // добавление
    public void addMessage(String roomUID, String text, final OnTransactionComplete<Void> onTransactionComplete) {
        MessageItem item = new MessageItem(UtilsDate.getNowDate(), currentUser.getUid(), currentUser.getDisplayName(), text);

        dbMessages.child(roomUID).push().runTransaction(
                        getAddItemTransactionHandler(item, onTransactionComplete));
    }

    // слушатели
    public void setMessageListener(String roomUID, final MessageItemValueListener eventListener) {
        if (messageListener == null)
            initMessageListener(eventListener);
        dbMessages.child(roomUID).addChildEventListener(messageListener);
    }
    public void removeMessageListener(String roomUID) {
        if (messageListener != null)
            dbMessages.child(roomUID).removeEventListener(messageListener);
    }
    private void initMessageListener(final MessageItemValueListener eventListener) {
        messageListener =  new LiteChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                MessageItem value = dataSnapshot.getValue(MessageItem.class);
                if ((value != null) && (value.getCreateDate() != null)) {
                    Utils.Log("on message add: " + value.toString());
                    eventListener.onAdd(value);
                }
            }
        };
    }

    public void getMessages(String roomUID, final MessageItemValueListener eventListener) {
        dbMessages.child(roomUID)
                .addListenerForSingleValueEvent(
                        new LiteValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                ArrayList<MessageItem> items = new ArrayList<>();
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    MessageItem item = snapshot.getValue(MessageItem.class);
                                    items.add(item);
                                }
                                if (items != null)
                                    Utils.Log("StorageManager: getMessages: ValueEventListener: " + items.size());
                                else
                                    Utils.Log("StorageManager: getMessages: ValueEventListener: items is null");
                                eventListener.onValue(items);
                            }
                        }
                );
    }
}
