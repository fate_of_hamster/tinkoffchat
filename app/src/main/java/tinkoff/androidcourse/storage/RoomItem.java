package tinkoff.androidcourse.storage;

import java.util.Date;

public class RoomItem {

    private String UID;
    private Date createDate;
    private String createUserUID;
    private String title;
    private String lastMessageText;

    public RoomItem() {
        // for firebase
    }

    public RoomItem(String UID, String title, String createUserUID, Date createDate, String lastMessageText) {
        this.UID = UID;
        this.createDate = createDate;
        this.createUserUID = createUserUID;
        this.title = title;
        this.lastMessageText = lastMessageText;
    }

    public RoomItem(String title, String createUserUID, Date createDate, String lastMessageText) {
        this.UID = null;
        this.createDate = createDate;
        this.createUserUID = createUserUID;
        this.title = title;
        this.lastMessageText = lastMessageText;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getTitle() {
        return title;
    }

    public String getLastMessageText() {
        return lastMessageText;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public String getCreateUserUID() {
        return createUserUID;
    }

    @Override
    public String toString() {
        return "{"
                + "UID:" + UID + "; "
                + "createDate:" + createDate + "; "
                + "createUserUID:" + createUserUID + "; "
                + "title:" + title + "; "
                + "lastMessageText:" + lastMessageText + "; "
                + "}";
    }
}
