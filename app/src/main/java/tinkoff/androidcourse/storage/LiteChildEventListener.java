package tinkoff.androidcourse.storage;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

public abstract class LiteChildEventListener implements ChildEventListener {
    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        // empty
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        // empty
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        // empty
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        // empty
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        throw databaseError.toException();
    }
}
