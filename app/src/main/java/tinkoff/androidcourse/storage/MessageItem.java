package tinkoff.androidcourse.storage;

import java.util.Date;

public class MessageItem {

    private Date   createDate;
    private String createUserUID;
    private String createUserName;
    private String text;

    public MessageItem() {
        // for firebase
    }

    public MessageItem(Date createDate, String createUserUID,  String createUserName, String text) {
        this.createDate = createDate;
        this.createUserUID = createUserUID;
        this.createUserName = createUserName;
        this.text = text;
    }

    @Override
    public String toString() {
        return "{"
                + "createDate: " + createDate + "; "
                + "createUserUID: " + createUserUID + "; "
                + "createUserName: " + createUserName + "; "
                + "text: " + text + "; "
                + "}";
    }

    public String getCreateUserName() {
        return createUserName;
    }


    public Date getCreateDate() {
        return createDate;
    }

    public String getCreateUserUID() {
        return createUserUID;
    }

    public String getText() {
        return text;
    }
}
