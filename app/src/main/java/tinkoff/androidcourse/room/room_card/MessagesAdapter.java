package tinkoff.androidcourse.room.room_card;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import tinkoff.androidcourse.dagger.ComponentInjecter;
import tinkoff.androidcourse.base.UtilsDate;
import tinkoff.androidcourse.storage.MessageItem;
import tinkoff.androidcourse.storage.StorageManager;
import tinkoff.androidcourse.ui.widget.WidgetCloudMessage;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.CustomViewHolder> {

    @Inject public StorageManager storageManager;
    @Inject Context context;

    private List<MessageItem> items;

    private static int MY_MESSAGE = 0;
    private static int OTHER_MESSAGE = 1;

    public MessagesAdapter() {
        ComponentInjecter.getComponent().inject(this);
    }

    public void setItems(List<MessageItem> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }
    public void addItem(MessageItem item) {
        if (this.items == null)
            this.items = new ArrayList<>();
        this.items.add(item);
        // наверное, тут бы ещё сортировка не помешала... но как же хочется спать
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).getCreateUserUID().equals(storageManager.getCurrentUserUID()))
            return MY_MESSAGE;
        else
            return OTHER_MESSAGE;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        WidgetCloudMessage widgetCloudMessage = new WidgetCloudMessage(context); // сюда бы ещё трибутов подкинуть
        widgetCloudMessage.setIsMyMessage(viewType == MY_MESSAGE);
        return new CustomViewHolder(widgetCloudMessage);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        MessageItem item = items.get(position);
        WidgetCloudMessage widgetCloudMessage = ((WidgetCloudMessage) holder.itemView);

        if (item != null) {
            widgetCloudMessage.setUserName(items.get(position).getCreateUserName());
            widgetCloudMessage.setText(items.get(position).getText());
            widgetCloudMessage.setCreateDate(UtilsDate.dateToStrForUser(item.getCreateDate()));
        }
    }

    @Override
    public int getItemCount() {
        if (items != null)
            return items.size();
        else
            return 0;
    }


    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        public CustomViewHolder(View v) {
            super(v);
        }
    }
}