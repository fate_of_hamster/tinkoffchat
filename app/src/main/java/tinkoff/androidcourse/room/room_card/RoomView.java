package tinkoff.androidcourse.room.room_card;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import java.util.List;

import tinkoff.androidcourse.storage.MessageItem;

public interface RoomView extends MvpView {
    void setMessages(List<MessageItem> items);
    void addMessage(MessageItem item);
    void showErrorMessage(String text);
}
