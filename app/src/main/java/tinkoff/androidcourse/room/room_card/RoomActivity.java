package tinkoff.androidcourse.room.room_card;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import tinkoff.androidcourse.R;
import tinkoff.androidcourse.base.Utils;
import tinkoff.androidcourse.storage.MessageItem;
import tinkoff.androidcourse.ui.widget.WidgetSendNewMessage;

public class RoomActivity extends MvpActivity<RoomView, RoomPresenter> implements RoomView  {

    private final static String ROOM_UID = "ROOM_UID";
    private final static String ROOM_TITLE = "ROOM_TITLE";

    private String roomUID;
    private LinearLayoutManager layoutManager;
    private MessagesAdapter adapter;

    @BindView(R.id.main_list) public RecyclerView recyclerView;
    @BindView(R.id.widget_send_message) public WidgetSendNewMessage sendNewMessageWidget;

    public static void start(String roomUID, String roomTitle, Context context) {
        Intent intent = new Intent(context, RoomActivity.class);
        intent.putExtra(ROOM_UID, roomUID);
        intent.putExtra(ROOM_TITLE, roomTitle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_activity);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            roomUID = bundle.getString(ROOM_UID, null);
            getSupportActionBar().setTitle(bundle.getString(ROOM_TITLE, null));
        }
        presenter.setRoomUID(roomUID);

        if (roomUID == null) {
            Toast.makeText(this, getResources().getString(R.string.error_room_not_found), Toast.LENGTH_SHORT).show();
            this.finish();
        }
        else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            initRecyclerView();

            sendNewMessageWidget.setOnButtonSendClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.addNewMessage(sendNewMessageWidget.getText().toString());
                    sendNewMessageWidget.clearText();
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.startListen();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.stopListen();
    }

    @NonNull
    @Override
    public RoomPresenter createPresenter() {
        return new RoomPresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null)
            presenter.refresh();
    }

    private void initRecyclerView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(false);
        layoutManager.setStackFromEnd(false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MessagesAdapter();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setMessages(List<MessageItem> items) {
        adapter.setItems(items);
        scroolMessagesToLast();
    }

    @Override
    public void addMessage(MessageItem item) {
        adapter.addItem(item);
        scroolMessagesToLast();
    }

    private void scroolMessagesToLast() {
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void showErrorMessage(String text) {
        Utils.showMessage(text, this);
    }
}
