package tinkoff.androidcourse.room.room_card;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;
import javax.inject.Inject;

import tinkoff.androidcourse.dagger.ComponentInjecter;
import tinkoff.androidcourse.storage.MessageItem;
import tinkoff.androidcourse.storage.StorageManager;
import tinkoff.androidcourse.storage.MessageItemValueListener;
import tinkoff.androidcourse.storage.OnTransactionComplete;

public class RoomPresenter extends MvpBasePresenter<RoomView> {

    @Inject public StorageManager storageManager;
    private MessageItemValueListener messageItemValueListener;
    private String roomUID;

    public RoomPresenter() {
        super();
        ComponentInjecter.getComponent().inject(this);
    }

    public void startListen() {
        storageManager.setMessageListener(roomUID, getMessageItemValueListener());
    }
    public void stopListen() {
        storageManager.removeMessageListener(roomUID);
    }


    public void setRoomUID(String roomUID) {
        this.roomUID = roomUID;
    }

    public void refresh() {
        storageManager.getMessages(roomUID, getMessageItemValueListener());
    }

    public void addNewMessage(String text) {
        OnTransactionComplete onTransactionComplete = new OnTransactionComplete() {
            @Override
            public void onCommit(Object result) {
                //refresh();
            }
            @Override
            public void onAbort(Exception e) {
                if (isViewAttached())
                    getView().showErrorMessage(e.toString());
            }
        };
        storageManager.addMessage(roomUID, text, onTransactionComplete);
    }


    @Override
    public void attachView(RoomView view) {
        super.attachView(view);
    }

    private MessageItemValueListener getMessageItemValueListener() {
        if (messageItemValueListener == null) {
            messageItemValueListener = new MessageItemValueListener() {
                @Override
                public void onValue(List<MessageItem> items) {
                    if (isViewAttached())
                        getView().setMessages(items);
                }
                @Override
                public void onAdd(MessageItem item) {
                    if (isViewAttached())
                        getView().addMessage(item);
                }
                @Override
                public void showMessage(String text) {
                    if (isViewAttached())
                        getView().showErrorMessage(text);
                }
            };
        }
        return messageItemValueListener;
    }
}