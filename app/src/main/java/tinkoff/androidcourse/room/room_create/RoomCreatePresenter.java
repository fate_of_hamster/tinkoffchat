package tinkoff.androidcourse.room.room_create;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import javax.inject.Inject;

import tinkoff.androidcourse.dagger.ComponentInjecter;
import tinkoff.androidcourse.storage.StorageManager;
import tinkoff.androidcourse.storage.OnTransactionComplete;

public class RoomCreatePresenter extends MvpBasePresenter<RoomCreateView> {

    @Inject public StorageManager storageManager;
    private boolean isLoadingProcess = false;

    public RoomCreatePresenter() {
        super();
        ComponentInjecter.getComponent().inject(this);
    }

    @Override
    public void attachView(RoomCreateView view) {
        super.attachView(view);
        setLoadingProcessToView();
    }

    public void addNewRoom(String name) {
        loadingStart();

        OnTransactionComplete onTransactionComplete = new OnTransactionComplete() {
            @Override
            public void onCommit(Object result) {
                loadingStop();
                if (isViewAttached())
                    getView().doFinish();
            }
            @Override
            public void onAbort(Exception e) {
                loadingStop();
                if (isViewAttached())
                    getView().showMessage(e.toString());
            }
        };

        storageManager.addRoom(name, onTransactionComplete);
    }

    private void loadingStart() {
        this.isLoadingProcess = true;
        setLoadingProcessToView();
    }
    private void loadingStop() {
        this.isLoadingProcess = false;
        setLoadingProcessToView();
    }
    public void setLoadingProcessToView() {
        if (isViewAttached()) {
            if (isLoadingProcess)
                getView().showLoadingStart();
            else
                getView().showLoadingStop();
        }
    }
}