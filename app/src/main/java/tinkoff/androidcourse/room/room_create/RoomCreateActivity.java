package tinkoff.androidcourse.room.room_create;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tinkoff.androidcourse.R;
import tinkoff.androidcourse.base.Utils;
import tinkoff.androidcourse.ui.widget.WidgetProgressButton;

public class RoomCreateActivity  extends MvpActivity<RoomCreateView, RoomCreatePresenter>  implements RoomCreateView  {

    @BindView(R.id.edt_name) public EditText edtName;
    @BindView(R.id.btn_create) public WidgetProgressButton btnCreate;


    public static void start(Context context) {
        Intent intent = new Intent(context, RoomCreateActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_create_activity);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle(getString(R.string.room_creating));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setLoadingProcessToView();
    }

    @NonNull
    @Override
    public RoomCreatePresenter createPresenter() {
        return new RoomCreatePresenter();
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_create)
    public void createRoom() {
        String name = edtName.getText().toString();
        if ((name != null) && (!name.isEmpty()))
            presenter.addNewRoom(name);
        else
            Utils.showMessage(R.string.room_need_name, this);
    }

    @Override
    public void doFinish() {
        finish();
    }

    @Override
    public void showMessage(String message) {
        Utils.showMessage(message, this);
    }


    @Override
    public void showLoadingStart() {
        Utils.hideKeyBoard(this);
        if (btnCreate != null)
            btnCreate.showProgress();
    }
    @Override
    public void showLoadingStop() {
        if (btnCreate != null)
            btnCreate.hideProgress();
    }
}
