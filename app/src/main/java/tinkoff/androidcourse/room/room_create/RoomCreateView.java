package tinkoff.androidcourse.room.room_create;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface RoomCreateView extends MvpView {
    void doFinish();

    void showMessage(String message);

    void showLoadingStart();
    void showLoadingStop();
}
