package tinkoff.androidcourse.room.room_dict;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tinkoff.androidcourse.R;
import tinkoff.androidcourse.base.Utils;
import tinkoff.androidcourse.storage.RoomItem;
import tinkoff.androidcourse.room.room_card.RoomActivity;
import tinkoff.androidcourse.room.room_create.RoomCreateActivity;

public class RoomDictFragment extends MvpFragment<RoomDictView, RoomDictPresenter> implements RoomDictView {

    public static final String TAG = "RoomDictFragment";
    public static final int NAME_STRING_ID = R.string.fragment_title_rooms_dict;

    @BindView(R.id.main_list) public RecyclerView recyclerView;
    private RoomDictAdapter adapter;

    public static RoomDictFragment newInstance() {
        RoomDictFragment fragment = new RoomDictFragment();
        return fragment;
    }

    @Override
    public RoomDictPresenter createPresenter() {
        return new RoomDictPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rooms_dict_fragment, container, false);
        ButterKnife.bind(this, view);
        initRecyclerView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.refresh(); // повторюсь, пока делаю грубо; позже продумаю оптимизацию; исхожу из логики "заказчик до 2го числа ждёт работающий прототип"
    }

    private void initRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        OnRoomDictClickListener onRoomDictClickListener = new OnRoomDictClickListener() {
            @Override
            public void onClick(int position) {
                RoomActivity.start(adapter.getItemByPosition(position).getUID(), adapter.getItemByPosition(position).getTitle(), getContext());
            }
        };
        adapter = new RoomDictAdapter(onRoomDictClickListener);
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    @OnClick(R.id.btn_add_room)
    public void addRoom() {
        RoomCreateActivity.start(getContext());
    }

    @Override
    public void setRooms(List<RoomItem> items) {
        adapter.setItems(items);
    }

    @Override
    public void showMessage(String text) {
        Utils.Log(text);
    }
}
