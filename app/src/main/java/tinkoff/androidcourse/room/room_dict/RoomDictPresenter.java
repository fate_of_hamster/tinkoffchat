package tinkoff.androidcourse.room.room_dict;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import java.util.List;
import javax.inject.Inject;
import tinkoff.androidcourse.dagger.ComponentInjecter;
import tinkoff.androidcourse.storage.StorageManager;
import tinkoff.androidcourse.storage.RoomItem;
import tinkoff.androidcourse.storage.RoomItemValueListener;

public class RoomDictPresenter extends MvpBasePresenter<RoomDictView> {

    @Inject public StorageManager storageManager;
    private RoomItemValueListener roomItemValueListener;

    public RoomDictPresenter() {
        super();
        ComponentInjecter.getComponent().inject(this);
        storageManager.setRoomListener(getRoomItemValueListener());
        refresh();
    }

    @Override
    public void attachView(RoomDictView view) {
        super.attachView(view);
    }

    public void refresh() {
        storageManager.getRooms(getRoomItemValueListener());
    }

    private RoomItemValueListener getRoomItemValueListener() {
        if (roomItemValueListener == null) {
            roomItemValueListener = new RoomItemValueListener() {
                @Override
                public void onValue(List<RoomItem> items) {
                    if (isViewAttached())
                        getView().setRooms(items);
                }

                @Override
                public void showMessage(String text) {
                    if (isViewAttached())
                        getView().showMessage(text);
                }
            };
        }
        return roomItemValueListener;
    }
}