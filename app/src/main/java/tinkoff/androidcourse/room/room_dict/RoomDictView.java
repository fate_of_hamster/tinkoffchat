package tinkoff.androidcourse.room.room_dict;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import java.util.List;
import tinkoff.androidcourse.storage.RoomItem;

public interface RoomDictView extends MvpView {
    void setRooms(List<RoomItem> items);
    void showMessage(String text);
}
