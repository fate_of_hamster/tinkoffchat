package tinkoff.androidcourse.room.room_dict;

public interface OnRoomDictClickListener {
    void onClick(int position);
}
