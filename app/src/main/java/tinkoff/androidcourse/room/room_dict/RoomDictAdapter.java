package tinkoff.androidcourse.room.room_dict;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import tinkoff.androidcourse.R;
import tinkoff.androidcourse.base.UtilsDate;
import tinkoff.androidcourse.storage.RoomItem;

public class RoomDictAdapter extends RecyclerView.Adapter<RoomDictAdapter.ViewHolder> {

    private List<RoomItem> items;
    private OnRoomDictClickListener clickListener;

    public RoomDictAdapter(OnRoomDictClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public RoomDictAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rooms_dict_item, parent, false);
        return new ViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(items.get(position).getTitle());
        holder.desc.setText(items.get(position).getLastMessageText()); // просто показываю текст последнего сообщения

        holder.createDate.setText(UtilsDate.dateToStr(items.get(position).getCreateDate()));
    }

    @Override
    public int getItemCount() {
        if (items == null)
            return 0;
        else
            return items.size();
    }


    public void setItems(List<RoomItem> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }
    public RoomItem getItemByPosition(int position) {
        return items.get(position);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lbl_title) public TextView title;
        @BindView(R.id.lbl_description) public TextView desc;
        @BindView(R.id.lbl_create_user) public TextView createUser;
        @BindView(R.id.lbl_create_date) public TextView createDate;

        public ViewHolder(View view, OnRoomDictClickListener listener) {
            super(view);
            ButterKnife.bind(this, view);
            setListener(listener);
        }

        private void setListener(final OnRoomDictClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onClick(getAdapterPosition());
                }
            });
        }
    }
}