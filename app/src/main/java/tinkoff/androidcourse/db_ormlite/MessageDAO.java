package tinkoff.androidcourse.db_ormlite;


import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;
import java.util.List;
import tinkoff.androidcourse.base.Utils;

public class MessageDAO extends BaseDaoImpl<Message, Integer> {

    protected MessageDAO(ConnectionSource connectionSource, Class<Message> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public Message get(int id) throws SQLException {
        QueryBuilder<Message, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Message.FIELD_ID, id);
        queryBuilder.limit(1L);
        Message message = queryForFirst(queryBuilder.prepare());
        return message;
    }

    public List<Message> getByRoom(Room room) throws SQLException {
        QueryBuilder<Message, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Message.FIELD_ROOM, room);
        PreparedQuery<Message> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    // возвращает id сообщения из комнаты, которое нужно отправить на сервер
    public Integer getByRoomAnotherToNeedSending(Room room) throws SQLException {
        QueryBuilder<Message, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Message.FIELD_ROOM, room)
                .and().eq(Message.FIELD_STATUS_ID, Message.STATUS_CREATE); // в статусе Создано
        queryBuilder.orderBy(Message.FIELD_CREATEDATE, true); // самое раннее по дате создания
        queryBuilder.limit(1L);
        Message message = queryForFirst(queryBuilder.prepare());
        if (message != null)
            return message.getId();
        return null;
    }

    public void setStatus(int id, int statusID) throws SQLException {
        UpdateBuilder<Message, Integer> updateBuilder = updateBuilder();
        updateBuilder.where().eq(Message.FIELD_ID, id);
        updateBuilder.updateColumnValue(Message.FIELD_STATUS_ID, statusID);
        updateBuilder.update();
    }

    public Message getLastByRoom(Room room) throws SQLException {
        QueryBuilder<Message, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Message.FIELD_ROOM, room);
        queryBuilder.orderBy(Message.FIELD_CREATEDATE, false);
        queryBuilder.limit(1L);
        return queryForFirst(queryBuilder.prepare());
    }

    //----------------------------------------------------------------------------------------------
    // я пока не уверенно пользуюсь даггером (нужно больше практики), так бы думаю он решил бы эту проблему
    // так что, пока просто делаю статические методы
    public static List<Message> getAllMessagesByRoom(Room room) {
        List<Message> m = null;
        try {
            m = HelperFactory.getHelper().getMessageDAO().getByRoom(room);
        } catch (SQLException e) {
            Utils.Log("Error geting messages by room_activity = " + room + ", with message: " + e.getMessage());
            e.printStackTrace();
        }
        return m;
    }

    public static int addMessage(Message message) {
        int newMessageID = -1;
        try {
            HelperFactory.getHelper().getMessageDAO().create(message);
            HelperFactory.getHelper().getRoomDAO().updateLastMessageText(message.getRoom());
            newMessageID = message.getId();
        } catch (SQLException e) {
            Utils.Log("Error creating new my message with message: " + e.getMessage());
            e.printStackTrace();
        }
        return newMessageID;
    }

    public static Integer getByRoomAnotherNotSended(Room room) {
        try {
            return HelperFactory.getHelper().getMessageDAO().getByRoomAnotherToNeedSending(room);
        } catch (SQLException e) {
            Utils.Log("Error getByRoomAnotherNotSended with message: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public static Boolean setStatusID(int id, int statusID) {
        if (id != -1) {
            try {
                HelperFactory.getHelper().getMessageDAO().setStatus(id, statusID);
            } catch (SQLException e) {
                Utils.Log("Error setting statusID (" + statusID + ") on message (" + id + ")  with message: " + e.getMessage());
                e.printStackTrace();
            }
            return true;
        }
        else
            return false;
    }
}
