package tinkoff.androidcourse.db_ormlite;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;
import java.util.Date;

import tinkoff.androidcourse.base.UtilsDate;

@DatabaseTable(tableName = "Room")
public class Room {

    public final static String FIELD_ID = "ID";
    public final static String FIELD_LAST_MESSAGE_TEXT = "LastMessageText";

    public Room() {
    }

    public Room(String caption) {
        this.caption = caption;
        this.createDate = UtilsDate.getNowDate();
    }

    @DatabaseField(
            generatedId = true,
            index = true,
            columnName = FIELD_ID)
    private int id;

    @DatabaseField(
            canBeNull = false,
            dataType = DataType.STRING,
            columnName = "Caption")
    private String caption;

    @DatabaseField(
            canBeNull = false,
            dataType = DataType.DATE,
            columnName = "CreateDate")
    private Date createDate;


    // вот эту колонку добавили в версии ДБ = 6, в версии 5 её не было
    @DatabaseField(
            dataType = DataType.STRING,
            columnName = FIELD_LAST_MESSAGE_TEXT)
    private String lastMessageText;

    @ForeignCollectionField(eager = true)
    private Collection<Message> messageList;


    public int getId() {
        return id;
    }

    public String getCaption() {
        return caption;
    }

    public String getLastMessageText() {
        return lastMessageText;
    }
}
