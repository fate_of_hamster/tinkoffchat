package tinkoff.androidcourse.db_ormlite;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
Пользователи
Работаю с ними по очень УПРОЩЁННОЙ схеме - просто ID и логин (который является сразу и именем)
 */

@DatabaseTable(tableName = "User")
public class User {

    public final static String FIELD_ID = "ID";
    public final static String FIELD_LOGIN = "Login";

    public User() {
    }

    public User(String login) {
        this.login = login;
    }

    @DatabaseField(
            generatedId = true,
            index = true,
            columnName = FIELD_ID)
    private int id;

    @DatabaseField(
            canBeNull = false,
            dataType = DataType.STRING,
            columnName = FIELD_LOGIN)
    private String login;

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }
}
