package tinkoff.androidcourse.db_ormlite;

import android.content.Context;
import com.j256.ormlite.android.apptools.OpenHelperManager;

/*
с ORMLite сейчас знакомился по большому счёту на основе статьи с хабра https://habrahabr.ru/post/143431/
HelperFactory посредник между HelperDatabase и нами - это возможность предотвратить утечки памяти из-за незакрытого соединения с БД
для обращения к методам класса RoomDao в любом классе приложения достаточно обратиться к классу-фабрике: RoomDAO roomsDao = HelperFactory.GetHelper().getRoomDAO();
// todo 20170430: надо подключиться к Dagger'у, но сейчас пока время ограничено и во многом надо разобраться, т.ч. буду пока использовать ORM без дагера
 */

public class HelperFactory{

    private static HelperDatabase databaseHelper;

    public static HelperDatabase getHelper(){
        return databaseHelper;
    }
    public static void setHelper(Context context){
        databaseHelper = OpenHelperManager.getHelper(context, HelperDatabase.class);
    }
    public static void releaseHelper(){
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }
}
