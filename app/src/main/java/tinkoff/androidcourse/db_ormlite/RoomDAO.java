package tinkoff.androidcourse.db_ormlite;


import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import tinkoff.androidcourse.base.Utils;

public class RoomDAO extends BaseDaoImpl<Room, Integer> {

    protected RoomDAO(ConnectionSource connectionSource, Class<Room> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Room> getAll() throws SQLException {
        return this.queryForAll();
    }

    public Room get(int id) throws SQLException {
        QueryBuilder<Room, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Room.FIELD_ID, id);
        queryBuilder.limit(1L);
        Room room = queryForFirst(queryBuilder.prepare());
        return room;
    }

    public void updateLastMessageText(Room room) throws SQLException {
        Message lastMessage = HelperFactory.getHelper().getMessageDAO().getLastByRoom(room);
        if (lastMessage != null) {
            UpdateBuilder<Room, Integer> updateBuilder = updateBuilder();
            updateBuilder.where().eq(Room.FIELD_ID, room.getId());
            updateBuilder.updateColumnValue(Room.FIELD_LAST_MESSAGE_TEXT, lastMessage.getText());
            updateBuilder.update();
        }
    }

    //----------------------------------------------------------------------------------------------
    // я пока не уверенно пользуюсь даггером (нужно больше практики), так бы думаю он решил бы эту проблему
    // так что, пока просто делаю статические методы
    public static Room getRoomByID(int id) {
        Room r = null;
        try {
            r = HelperFactory.getHelper().getRoomDAO().get(id);
        } catch (SQLException e) {
            Utils.Log("Error geting room_activity by id = " + id + ", with message: " + e.getMessage());
            e.printStackTrace();
        }
        return r;
    }
    public static List<Room> getAllRooms() {
        List<Room> result = null;
        try {
            result = HelperFactory.getHelper().getRoomDAO().getAll();
        } catch (SQLException e) {
            Utils.Log("Error reading all rooms with message: " + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
    public static void addRoom(String caption) {
        try {
            Room room = new Room(caption);
            HelperFactory.getHelper().getRoomDAO().create(room);
        } catch (SQLException e) {
            Utils.Log("Error creating new room_activity with message: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
