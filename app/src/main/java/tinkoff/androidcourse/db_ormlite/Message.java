package tinkoff.androidcourse.db_ormlite;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "Message")
public class Message {

    public final static String FIELD_ID = "ID";
    public final static String FIELD_ROOM = "Room";
    public final static String FIELD_STATUS_ID = "STATUS_ID";
    public final static String FIELD_CREATEDATE = "CreateDate";

    // возможно, лучше сделать отдельную табличку для статусов, но пока не усложняю
    public final static int STATUS_CREATE = 0;
    public final static int STATUS_SENDING = 1;
    public final static int STATUS_SENDED = 2;
    public final static int STATUS_NOT_SENDED = 3;

    public Message() {
    }

    public Message(Date createDate, int createUserID, String text, Room room) {
        this.createDate = createDate;
        this.createUserID = createUserID;
        this.text = text;
        this.room = room;
        this.statusID = STATUS_CREATE;
    }

    @DatabaseField(
            generatedId = true,
            index = true,
            columnName = FIELD_ID)
    private int id;

    @DatabaseField(
            canBeNull = false,
            dataType = DataType.DATE,
            columnName = FIELD_CREATEDATE)
    private Date createDate;

    @DatabaseField(
            canBeNull = false,
            dataType = DataType.INTEGER,
            columnName = "CreateUserID")
    private int createUserID;
    // пока буду хранить просто ID пользователя
    // (главная причина - надо разобраться как это работает под капотом, но пока не успеваю),
    // потом сделаю foreign и буду хранить ссылку на экземпляр, если не будет проблем

    @DatabaseField(
            canBeNull = false,
            dataType = DataType.STRING,
            columnName = "Text")
    private String text;

    @DatabaseField(
            canBeNull = false,
            foreign = true,
            foreignAutoRefresh = true,
            columnName = FIELD_ROOM)
    private Room room;
    public void setRoom(Room value){
        this.room = value;
    }
    public Room getRoom(){
        return room;
    }

    @DatabaseField(
            canBeNull = false,
            dataType = DataType.INTEGER,
            columnName = FIELD_STATUS_ID)
    private int statusID;

    public int getId() {
        return id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public int getCreateUserID() {
        return createUserID;
    }

    public String getText() {
        return text;
    }

    public int getStatusID() {
        return statusID;
    }
    public void setStatusID(int value) {
        this.statusID = value;
    }
}
