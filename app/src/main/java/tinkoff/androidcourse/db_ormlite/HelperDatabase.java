package tinkoff.androidcourse.db_ormlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.List;

import tinkoff.androidcourse.base.Utils;


public class HelperDatabase extends OrmLiteSqliteOpenHelper {

    private static final String TAG = HelperDatabase.class.getSimpleName();
    private static final String DATABASE_NAME ="hamsterDataBase.db"; //имя файла базы данных который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
    private static final int DATABASE_VERSION = 6; //с каждым увеличением версии, при нахождении в устройстве БД с предыдущей версией будет выполнен метод onUpgrade();

    private RoomDAO roomDAO = null; //ссылки на DAO соответсвующие сущностям, хранимым в БД
    private MessageDAO messageDAO = null;
    private UserDAO userDAO = null;

    public HelperDatabase(Context context){
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }


    // Выполняется, когда файл с БД не найден на устройстве
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource){
        try {
            TableUtils.createTable(connectionSource, Room.class);
            TableUtils.createTable(connectionSource, Message.class);
            TableUtils.createTable(connectionSource, User.class);
        }
        catch (SQLException e){
            Utils.Log("error creating DB " + DATABASE_NAME + " with message: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    // Выполняется, когда БД имеет версию отличную от текущей
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer){
        try {
            // в версии ДБ №6 была добавлена колонка "Текст последнего сообщения"
            if (oldVer < 6) {
                HelperFactory.getHelper().getRoomDAO().executeRaw("ALTER TABLE `Room` ADD COLUMN " + Room.FIELD_LAST_MESSAGE_TEXT + " STRING;"); // не нашёл как иначе, кроме как сырым запросом

                // да, это жесть, это надо оптимиизирвоать, не успеваю физически ;( (слишком много другой работы было в последние две недели)
                List<Room> rooms = HelperFactory.getHelper().getRoomDAO().getAll();
                for (int i = 0; i < rooms.size(); i++) {
                    HelperFactory.getHelper().getRoomDAO().updateLastMessageText(rooms.get(i));
                }
            }
        }
        catch (SQLException e){
            Utils.Log("Error upgrading db " + DATABASE_NAME + " from version " + oldVer + " to version " + newVer + " with message: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    //выполняется при закрытии приложения
    @Override
    public void close(){
        super.close();
        roomDAO = null;
        messageDAO = null;
        userDAO = null;
    }

    //-------------------- DAO
    public RoomDAO getRoomDAO() throws SQLException{
        if (roomDAO == null) {
            roomDAO = new RoomDAO(getConnectionSource(), Room.class);
        }
        return roomDAO;
    }

    public MessageDAO getMessageDAO() throws SQLException{
        if (messageDAO == null) {
            messageDAO = new MessageDAO(getConnectionSource(), Message.class);
        }
        return messageDAO;
    }

    public UserDAO getUserDAO() throws SQLException{
        if (userDAO == null) {
            userDAO = new UserDAO(getConnectionSource(), User.class);
        }
        return userDAO;
    }
}