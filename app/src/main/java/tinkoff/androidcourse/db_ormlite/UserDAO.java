package tinkoff.androidcourse.db_ormlite;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;

public class UserDAO extends BaseDaoImpl<User, Integer> {

    protected UserDAO(ConnectionSource connectionSource, Class<User> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }


    public User get(int id) throws SQLException {
        QueryBuilder<User, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(User.FIELD_ID, id);
        queryBuilder.limit(1L);
        User user = queryForFirst(queryBuilder.prepare());
        return user;
    }

    public User getByLogin(String login) throws SQLException {
        QueryBuilder<User, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(User.FIELD_LOGIN, login);
        queryBuilder.limit(1L);
        User user = queryForFirst(queryBuilder.prepare());
        return user;
    }

    public Integer createDistinctUserByLogin(String login) throws SQLException {
        User user = getByLogin(login);
        if (user == null) {
            create(new User(login));
            user = getByLogin(login);
        }

        if (user != null)
            return user.getId();
        else
            return null;
    }

    //----------------------------------------------------------------------------------------------
    // я пока не уверенно пользуюсь даггером (нужно больше практики), так бы думаю он решил бы эту проблему
    // так что, пока просто делаю статические методы
    public static String getUserLoginByID(int userID) {
        try {
            User user = HelperFactory.getHelper().getUserDAO().get(userID);
            if (user != null)
                return user.getLogin();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Integer saveUser(String login) {
        Integer userID = null;
        try {
            userID = HelperFactory.getHelper().getUserDAO().createDistinctUserByLogin(login);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userID;
    }
}
